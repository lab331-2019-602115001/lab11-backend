package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseAnotherService;

@Controller
@Slf4j
public class CourseAnotherController {
    @Autowired
    CourseAnotherService courseAnotherService;

    @GetMapping("/courseWithStudentMoreThan/{students}")
    public ResponseEntity getCourseWithStudentMoreThan(@PathVariable int students) {
        log.info("the another course controller is called");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(this.courseAnotherService.getCourseWhichStudentEnrolledMoreThan(students)));

    }
}
