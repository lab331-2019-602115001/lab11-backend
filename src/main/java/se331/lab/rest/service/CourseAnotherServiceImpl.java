package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.entity.Course;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseAnotherServiceImpl implements CourseAnotherService{
    @Autowired
    CourseAnotherDao courseAnotherDao;

    @Override
    public List<Course> getCourseWhichStudentEnrolledMoreThan(int n) {
        List<Course> courses = courseAnotherDao.getAllCourse();
        List<Course> output = new ArrayList<>();
        for (Course course : courses) {
            int numberOfStudent = course.getStudents().size();
            if (numberOfStudent > n) {
                output.add(course);
            }
        }
        return output;
    }
}
